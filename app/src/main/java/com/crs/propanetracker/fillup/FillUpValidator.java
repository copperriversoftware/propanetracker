package com.crs.propanetracker.fillup;

import android.database.Cursor;
import android.util.Log;

import com.crs.propanetracker.FillUp;
import com.crs.propanetracker.FillUpDAO;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FillUpValidator {

    private static final String TAG = FillUpValidator.class.getSimpleName();
    public static final String VALID_ENTRY = "Entry successfully recorded.";
    private static final SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    public static final String OLD_FILLUP_NOT_SUPPORTED = "entering a fill-up occurring before existing daily entries is not yet supported.";

    public static String validateEntry(Cursor cursor, FillUp fillUp) {
        String returnValue = "False";

        cursor.moveToFirst();
        Log.d(TAG, "Validating Fill Up: " + fillUp.toString());
        Log.d(TAG, "Validating against cursor with " + cursor.getCount() + " entries");

        Date fillUpDate = fillUp.getDate();

        switch (cursor.getCount()) {
            case 0:
                //This is the first entry, so no need to validate
                Log.d(TAG, "This is the first entry, so no need to validate.");

                //Everything passed, so lets explicitly set valid to true now.
                returnValue = VALID_ENTRY;
                break;
            case 1:
                //This case occurs when the query returns one entry.  Comments in individual IF/ELSE statements below
                FillUp fillUpFromCursor = FillUpDAO.cursorToFillUp(cursor);
                Date cursorDate = fillUpFromCursor.getDate();
                if (fillUpDate.after(cursorDate)) {
                    returnValue = OLD_FILLUP_NOT_SUPPORTED;
                    return returnValue;
                }

                //Everything passed, so lets explicitly set valid to true now.
                returnValue = VALID_ENTRY;
                break;
            case 2:
                //There are existing entries in the DB before and after this new entry
                returnValue = OLD_FILLUP_NOT_SUPPORTED;
                Log.d(TAG, returnValue);
                break;

        }
        Log.d(TAG, returnValue);
        return returnValue;
    }
}
