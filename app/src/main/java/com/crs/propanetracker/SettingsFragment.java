package com.crs.propanetracker;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crs.propanetracker.importData.ImportFragment;


public class SettingsFragment extends Fragment {

    private static final String TAG = SettingsFragment.class.getSimpleName();
    private static final String IMPORT_FRAGMENT = "ImportFragment";

    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of this fragment using the provided parameters.
     *
     * @return A new instance of fragment SettingsFragment.
     */
    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        TextView importDataView = (TextView) rootView.findViewById(R.id.importDataLabel);


        importDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                ImportFragment nextFrag = ImportFragment.newInstance();
                Fragment frag = getActivity().getSupportFragmentManager().findFragmentByTag(IMPORT_FRAGMENT);
                if (frag == null) {
                    Log.d(TAG, "Create a new instance of Import Fragment.");
                    frag = ImportFragment.newInstance();
                    fragmentTransaction.addToBackStack(IMPORT_FRAGMENT)
                            .replace(R.id.container, frag, IMPORT_FRAGMENT)
                            .commit();


//                    getActivity().getSupportFragmentManager().beginTransaction()
//                            .replace(R.id.container, frag, IMPORT_FRAGMENT)
//                            .addToBackStack(IMPORT_FRAGMENT)
//                            .commit();
                } else {
                    Log.d(TAG, "Found an instance of Import Fragment.");
//                    getActivity().getSupportFragmentManager().popBackStack(IMPORT_FRAGMENT, 0);
                    fragmentTransaction//.addToBackStack(IMPORT_FRAGMENT)
                            .replace(R.id.container, frag, IMPORT_FRAGMENT)
                            .commit();
                }

                getActivity().getSupportFragmentManager().executePendingTransactions();

            }
        });

        //FOR TESTING ONLY!!!!!!!!!!!!!!!!!!!!!!!
        rootView.findViewById(R.id.deleteAllFillUpsLabel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Delete all fill ups pressed");
                FillUpDAO fillUpDAO = new FillUpDAO(getContext());
                fillUpDAO.getAsyncQueryHandler().startDelete(FillUpDAO.DELETE_ALL_ENTRIES, null,
                        Uri.parse(FillUpDAO.URL), null, null);
            }
        });

        //FOR TESTING ONLY!!!!!!!!!!!!!!!!!!!!!!!
        rootView.findViewById(R.id.deleteAllDailyEntriesLabel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Delete all daily entries pressed");
                DailyEntryDAO dailyEntryDAO = new DailyEntryDAO(getContext());
                dailyEntryDAO.getAsyncQueryHandler().startDelete(DailyEntryDAO.DELETE_ALL_ENTRIES, null,
                        Uri.parse(DailyEntryDAO.URL), null, null);
            }
        });

        return rootView;
    }

}
