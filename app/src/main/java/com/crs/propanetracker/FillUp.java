package com.crs.propanetracker;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FillUp implements Parcelable {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    private long id;
    private Calendar calendar = Calendar.getInstance();
    private Date date;
    private int levelBeforeFillUp;
    private int levelAfterFillUp;
    private String notes;
    private float volume;
    private float price;


    public FillUp() {
        date = new Date();

        Date temp = new Date(calendar.getTimeInMillis());
    }

    public FillUp(Parcel in) {
        id = in.readLong();
        date = new Date(in.readLong());
        levelBeforeFillUp = in.readInt();
        levelAfterFillUp = in.readInt();
        notes = in.readString();
        volume = in.readFloat();
        price = in.readFloat();
    }

    public String getNotes() {

        return notes;
    }

    public void setNotes(String notes) {

        this.notes = notes;
    }

    public String getDateAsText() {

        return dateFormat.format(date);
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {

        return date;
    }

    public void setDate(Date date) {

        this.date = date;
    }

    public int getLevelBeforeFillUp() {

        return levelBeforeFillUp;
    }

    public void setLevelBeforeFillUp(int levelBeforeFillUp) {

        this.levelBeforeFillUp = levelBeforeFillUp;
    }

    public int getLevelAfterFillUp() {

        return levelAfterFillUp;
    }

    public void setLevelAfterFillUp(int levelAfterFillUp) {

        this.levelAfterFillUp = levelAfterFillUp;
    }

    public Calendar getCalendar() {

        return calendar;
    }

    public void setCalendar(Calendar calendar) {

        this.calendar = calendar;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "FillUp{" +
                "\n  date=" + getDateAsText() +
                "\n  levelBefore=" + levelBeforeFillUp +
                "\n  levelAfter=" + levelAfterFillUp +
                "\n  volume=" + volume +
                "\n  price=" + price +
                "\n  notes='" + notes + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeLong(date.getTime());
        parcel.writeInt(levelBeforeFillUp);
        parcel.writeInt(levelAfterFillUp);
        parcel.writeString(notes);
        parcel.writeFloat(volume);
        parcel.writeFloat(price);
    }

    // This is to de-serialize the object
    public static final Parcelable.Creator<FillUp> CREATOR = new Parcelable.Creator<FillUp>() {
        public FillUp createFromParcel(Parcel in) {
            return new FillUp(in);
        }

        @Override
        public FillUp[] newArray(int i) {
            return new FillUp[0];
        }
    };


}