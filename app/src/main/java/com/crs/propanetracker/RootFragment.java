package com.crs.propanetracker;

import android.support.v4.app.Fragment;

import com.crs.propanetracker.com.crs.propanetracker.db.DatePickerCallbackInterface;

/**
 * Created by cbeaudin on 5/14/2015.
 */
public abstract class RootFragment extends Fragment implements DatePickerCallbackInterface {

}
