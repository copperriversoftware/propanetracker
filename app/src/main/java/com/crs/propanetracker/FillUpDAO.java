package com.crs.propanetracker;

import android.annotation.SuppressLint;
import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.crs.propanetracker.com.crs.propanetracker.db.DbHelper;
import com.crs.propanetracker.com.crs.propanetracker.db.PropaneContentProvider;
import com.crs.propanetracker.fillup.FillUpValidator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FillUpDAO extends AsyncQueryHandler {
    private static final String TAG = FillUpDAO.class.getSimpleName();
    private Context context;
    static final String DAILY_ENTRIES_SUCCESS = "daily_entries_success";
    private static final String SUCCESS = "Entry successfully recorded.";
    public static final int NEW_FILL_UP = 2;
    public static final int INSERT_ONE = 3;
    public static final int DELETE_ALL_ENTRIES = 16;

    public static final String URL = "content://" + PropaneContentProvider.PROVIDER_NAME + "/" + DbHelper.TABLE_FILL_UPS;

    public static String[] ALL_COLUMNS = {DbHelper.COLUMN_ID, DbHelper.COLUMN_DATE, DbHelper.COLUMN_LEVEL, DbHelper.COLUMN_NOTES, DbHelper.COLUMN_PRICE, DbHelper.COLUMN_VOLUME};


    public FillUpDAO(final Context context) {
        super(context.getContentResolver());
        this.context = context;
    }


//    /**
//     * Validates that the fill-up is not in the future.
//     *
//     * @param date
//     * @param level
//     * @return
//     */
//    private String validateFillUpEntry(Date date, int level) {
//        String returnValue = "True";
//
//        Log.d(TAG, "Validating Fill-Up with date: " + date + " & Level: " + level);
//
//        //Validate not a future date
//        Date currentDate = Calendar.getInstance().getTime();
//        if (date.after(currentDate)) {
//            return "Fill-Up's cannot be in the future.";
//        }
//
//        List<FillUp> allFillUps = getAllFillUpsNewestFirst();
//        if (allFillUps.isEmpty()) {
//            //This is the first entry, so no need to validate
//            return "True";
//        }
//
//        return returnValue;
//    }

//    public List<FillUp> getAllFillUps() {
//        List<FillUp> fillUps = new ArrayList<FillUp>();
//
//        Cursor cursor = database.query(DbHelper.TABLE_FILL_UPS, ALL_COLUMNS, null, null, null, null, null);
//        cursor.moveToFirst();
//
//        while (!cursor.isAfterLast()) {
//            FillUp fillUp = cursorToFillUp(cursor);
//            fillUps.add(fillUp);
//            cursor.moveToNext();
//        }
//
//        cursor.close();
//        Log.d(TAG, "Number of FillUps: " + fillUps.size());
//        return fillUps;
//    }

//    //TODO test me out.
//    private List<FillUp> getAllFillUpsNewestFirst() {
//        List<FillUp> fillUps = new ArrayList<FillUp>();
//
//        Cursor cursor = database.query(DbHelper.TABLE_FILL_UPS, ALL_COLUMNS, null, null, null, null, DbHelper.COLUMN_DATE + " DESC");
//        while (!cursor.isAfterLast()) {
//            FillUp fillUp = cursorToFillUp(cursor);
//            fillUps.add(fillUp);
//            cursor.moveToNext();
//        }
//
//        cursor.close();
//        Log.d(TAG, "Number of FillUps: " + fillUps.size());
//        return fillUps;
//    }

    private void printAllEntries(Cursor cursor) {
        cursor.moveToFirst();

        Log.d(TAG,
                "-------  Printing all (" + cursor.getCount() + ") Fill Ups in chronological order (oldest -> newest)  -------");

        while (!cursor.isAfterLast()) {
            FillUp fillUp = FillUpDAO.cursorToFillUp(cursor);
            Log.d(TAG, fillUp.toString());
            cursor.moveToNext();
        }

        Log.d(TAG, "----------   Finished Printing Fill Ups .     ----------");

        cursor.moveToFirst();
    }

//    private void deleteEntry(FillUp fillUp) {
//        long id = fillUp.getId();
//        Log.d(TAG, "Deleted entry with id: " + id);
//        int numRowsDeleted = database.delete(DbHelper.TABLE_FILL_UPS, DbHelper.COLUMN_ID + " = " + id, null);
//        if (numRowsDeleted == 1) {
//            Log.d(TAG, "Successfully deleted " + numRowsDeleted + "row");
//        } else {
//            Log.e(TAG, "Something went wrong.  Instead of deleting one row we deleted: " + numRowsDeleted);
//        }
//    }

    public synchronized static FillUp cursorToFillUp(Cursor cursor) {
        FillUp fillUp = new FillUp();
        //getting android.database.CursorIndexOutOfBoundsException: Index -1 requested, with a size of 2
        Log.d(TAG, "cursor.getCount (# of fill-ups?): " + cursor.getCount());
        fillUp.setId(cursor.getLong(0));
        Date date = new Date(cursor.getLong(1));
        fillUp.setDate(date);
        fillUp.setLevelBeforeFillUp(cursor.getInt(2));
        fillUp.setNotes(cursor.getString(3));
        fillUp.setPrice(cursor.getInt(4));
        fillUp.setVolume(cursor.getInt(5));

        return fillUp;
    }

    private void displayToast(String message) {
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

    @SuppressLint("HandlerLeak")
    @NonNull
    public AsyncQueryHandler getAsyncQueryHandler() {
        return new AsyncQueryHandler(context.getContentResolver()) {
            @Override
            protected void onInsertComplete(int token, Object cookie, Uri uri) {
                displayToast("Fill-up successfully saved.");
                Log.d(TAG, "Just inserted: " + uri);
            }

            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                Log.d(TAG, "onQueryComplete completed. Token value: " + token);
                Log.d(TAG, "Is cursor last: " + cursor.isLast());
                Log.d(TAG, "Cursor Count: " + cursor.getCount());

                List<FillUp> fillEntries = new ArrayList<>();

                switch (token) {
                    case NEW_FILL_UP:

                        //Add logic for two necessary daily entries???

                        FillUp newFillUp = ((FillUp) cookie);
                        areDailyEntriesValid(newFillUp);

                        insertFillUpIfValid(cursor, newFillUp);

                        //DEBUG
                        printAllEntries(cursor);
                        cursor.close();
                        break;
                }

            }

            @Override
            protected void onDeleteComplete(int token, Object cookie, int result) {
                showToast("Deleted all Fill Ups");
            }
        };
    }

    private void areDailyEntriesValid(FillUp fillUp) {
        DailyEntryDAO dailyEntryDAO = new DailyEntryDAO(context);

        DbHelper dbHelper = DbHelper.getInstance(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        //Make DailyEntry for the values before the fillup
        DailyEntry dailyEntryBeforeFillup = new DailyEntry();
        dailyEntryBeforeFillup.setLevel(Integer.valueOf(fillUp.getLevelBeforeFillUp()));
        dailyEntryBeforeFillup.setDate(fillUp.getDate());

        Cursor cursor = database.query(DbHelper.TABLE_HISTORY, DbHelper.HISTORY_TABLE_ALL_COLUMNS,
                DbHelper.generateDailyEntrySelectionClause(fillUp.getDate().getTime()), null, null,
                null, "entryDate ASC");
        dailyEntryDAO.insertDailyEntryIfValid(cursor, dailyEntryBeforeFillup);


        //Make DailyEntry for the values before the fillup
        DailyEntry dailyEntryAfterFillup = new DailyEntry();
        dailyEntryAfterFillup.setLevel(Integer.valueOf(fillUp.getLevelAfterFillUp()));
        dailyEntryAfterFillup.setDate(fillUp.getDate());
        dailyEntryAfterFillup.setIsEntryAfterFillUp(true);

        dailyEntryDAO.insertDailyEntryIfValid(cursor, dailyEntryAfterFillup);
    }

    private void insertFillUpIfValid(Cursor cursor, FillUp fillUp) {

        if (cursor == null || cursor.getCount() == 0) {
            //First entry in the DB, so we don't need to validate this newEntry.  Just insert it.
            insertEntry(fillUp);
            displayToast("First Fill Up");
        } else {
            String isValidFillUp = FillUpValidator.validateEntry(cursor, fillUp);
            if (isValidFillUp.equalsIgnoreCase(FillUpValidator.VALID_ENTRY)) {
                insertEntry(fillUp);
                displayToast("Not the first Fill Up");
            }
        }
    }

    private void showToast(String msg) {
        if (msg.equalsIgnoreCase(SUCCESS)) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }

    private void insertEntry(FillUp fillUp) {
        ContentValues values = new ContentValues();
        values.put(DbHelper.COLUMN_DATE, fillUp.getDate().getTime());
        values.put(DbHelper.COLUMN_LEVEL, fillUp.getLevelAfterFillUp());
        values.put(DbHelper.COLUMN_VOLUME, fillUp.getVolume());
        values.put(DbHelper.COLUMN_PRICE, fillUp.getPrice());
        values.put(DbHelper.COLUMN_NOTES, fillUp.getNotes());

        Log.d(TAG, "Calling startInsert for new Fill Up: " + fillUp.toString());
        startInsert(INSERT_ONE, fillUp, Uri.parse(URL), values);
    }


}
