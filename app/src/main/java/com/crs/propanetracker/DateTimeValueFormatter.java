package com.crs.propanetracker;

import android.content.Context;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by ninja on 1/25/17.
 */

public class DateTimeValueFormatter  implements IAxisValueFormatter {
    private Context context;

    public DateTimeValueFormatter(Context context) {
        this.context = context;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        Date date = new Date((long)value);
        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(context);
        return dateFormat.format(date);
    }
}
