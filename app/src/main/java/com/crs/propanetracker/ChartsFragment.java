package com.crs.propanetracker;

import android.content.AsyncQueryHandler;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.crs.propanetracker.com.crs.propanetracker.db.DbHelper;
import com.crs.propanetracker.com.crs.propanetracker.db.PropaneContentProvider;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ninja on 1/25/17.
 */
public class ChartsFragment extends Fragment {

    private static final String TAG = ChartsFragment.class.getSimpleName();

    private LineChart chart;
    private DateTimeValueFormatter formatter;
    private AsyncQueryHandler pQueryHandler;
    private static final int DAILY_ENTRY_LIST = 1;

    private String[] allColumns = {DbHelper.COLUMN_ID, DbHelper.COLUMN_DATE, DbHelper.COLUMN_LEVEL, DbHelper.COLUMN_NOTES};

    /**
     * Use this factory method to create a new instance of this fragment using the provided parameters.
     *
     * @return A new instance of fragment DailyEntryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChartsFragment newInstance() {
        ChartsFragment fragment = new ChartsFragment();
        return fragment;
    }

    public ChartsFragment() {
        //Required empty constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        pQueryHandler = new AsyncQueryHandler(getContext().getContentResolver()) {
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                super.onQueryComplete(token, cookie, cursor);

                List<DailyEntry> dailyEntries = new ArrayList<>();


                try {
                    while (cursor.moveToNext()) {
                        DailyEntry dailyEntry = DailyEntryDAO.cursorToDailyEntry(cursor);
                        dailyEntries.add(dailyEntry);
                    }

                    if (dailyEntries.size() > 0) {
                        refreshChart(dailyEntries);
                    }
                } finally {
                    if (cursor != null && !cursor.isClosed()) {
                        cursor.close();
                    }
                }
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_charts, container, false);

        chart = (LineChart) rootView.findViewById(R.id.chart);

        String url = "content://" + PropaneContentProvider.PROVIDER_NAME + "/" + DbHelper.TABLE_HISTORY;
        pQueryHandler.startQuery(DAILY_ENTRY_LIST, null, Uri.parse(url), allColumns, null,
                null, DbHelper.COLUMN_DATE);

        return rootView;
    }

    private void refreshChart(List<DailyEntry> myEntries) {

        List<Entry> entries = new ArrayList<Entry>();

        for (DailyEntry data : myEntries) {

            // turn your data into Entry objects
            entries.add(new Entry(Float.parseFloat(String.valueOf(data.getDate().getTime())), data.getLevel()));
        }

        LineDataSet dataSet = new LineDataSet(entries, getString(R.string.x_axis_label)); // add entries to dataset
        dataSet.setColor(Color.rgb(255, 140, 0));
        dataSet.setCircleRadius(6);
        dataSet.setCircleColor(Color.rgb(210, 105, 30));
        dataSet.setCircleHoleRadius(2);
        dataSet.setLineWidth(2);
        dataSet.setValueTextColor(Color.BLACK); // styling, ...
        dataSet.setValueTextSize(12);

        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);

        formatter = new DateTimeValueFormatter(getContext());

        XAxis xAxis = chart.getXAxis();
        xAxis.setValueFormatter(formatter);

        Description description = new Description();
        description.setText("Propane usage by date and percentage.");
        chart.setDescription(description);

//        chart.animateX(3000);
        chart.animateY(2000, Easing.EasingOption.EaseInOutSine);

        chart.invalidate(); // refresh
    }
}
