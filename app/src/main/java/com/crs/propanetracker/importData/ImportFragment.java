package com.crs.propanetracker.importData;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crs.propanetracker.R;

public class ImportFragment extends Fragment {

    private static final String TAG = ImportFragment.class.getSimpleName();

    public ImportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of this fragment using the provided parameters.
     *
     * @return A new instance of fragment SettingsFragment.
     */
    public static ImportFragment newInstance() {
        ImportFragment fragment = new ImportFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_import, container, false);


        TextView cancelDataView = (TextView) rootView.findViewById(R.id.cancelImport);
        cancelDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Cancel selected");
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        });

        TextView startImportDataView = (TextView) rootView.findViewById(R.id.startImport);
        startImportDataView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Import Data Selected");
                CsvImporter csvImporter = new CsvImporter(getContext());
                csvImporter.importFromFile();
            }
        });

        return rootView;
    }
}
