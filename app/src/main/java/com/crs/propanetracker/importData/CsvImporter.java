package com.crs.propanetracker.importData;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.crs.propanetracker.DailyEntry;
import com.crs.propanetracker.DailyEntryDAO;
import com.crs.propanetracker.DailyEntryFragment;
import com.crs.propanetracker.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

public class CsvImporter {
    private static String TAG = CsvImporter.class.getSimpleName();

    private DailyEntryFragment dailyEntryFragment;
    private String csvFile = "/DailyEntries.csv/";
    private Context context;
    private int numberOfColumnsToImport = 3;


    public CsvImporter(Context context) {
        this.context = context;
    }


    public void importFromFile() {
        FileReader fileReader = null;
        DailyEntryDAO dailyEntryDAO = new DailyEntryDAO(context);

        File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS + csvFile);
        Log.d(TAG, "exists: " + downloadDir.exists());
        Log.d(TAG, "isReadable: " + downloadDir.canRead());
        Log.d(TAG, "path: " + downloadDir.getAbsolutePath());

        if (!downloadDir.exists()) {
            //File does not exist
            Toast.makeText(context, R.string.missingFile, Toast.LENGTH_LONG).show();
            return;
        }
        try {
            fileReader = new FileReader(downloadDir);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Error creating ..." + e);
        }

        BufferedReader buffer = new BufferedReader(fileReader);
        String line = "";
        int numberOfValidRows = 0;
        int numberOfSkippedRows = 0;
        try {
            while ((line = buffer.readLine()) != null) {
                String[] columns = line.split(",");
                //We use '-1' here because we don't want to require the 'Notes' column for import.  All that is required is 2 fileds (date and %).
                if (columns.length < (numberOfColumnsToImport - 1)) {
                    Log.d("CSVParser", "Skipping This Bad CSV Row: " + line);
                    numberOfSkippedRows++;
                    continue;
                }
                Log.d(TAG, "Processing line " + line);
                DailyEntry newEntry = new DailyEntry();
                try {
                    newEntry.setDate(columns[0].trim());

                    newEntry.setLevel(Integer.parseInt(columns[1].toString().trim()));

                    if (columns.length != (numberOfColumnsToImport - 1)) {
                        newEntry.setNotes(columns[2]);
                    }
                    Log.d(TAG, "Finished Daily Entry " + newEntry.toString());

//                    dailyEntryFragment.insertIfValid(dailyEntry);
                    dailyEntryDAO.getAsyncQueryHandler().startQuery(DailyEntryDAO.NEW_DAILY_ENTRY, newEntry, Uri.parse(DailyEntryDAO.URL), DailyEntryDAO.ALL_COLUMNS, null, null, "_ID DESC");
                    numberOfValidRows++;

                } catch (ParseException e) {
                    //Not a valid date, so we log it and don't do anything else with this row
                    Log.e(TAG, "Error getting date from string: " + columns[0].trim(), e);
                }


            }
            Toast.makeText(context, "     Imported (" + numberOfValidRows + ") entries. \n" +
                    "Skipped (" + numberOfSkippedRows + ") invalid entries.", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Total # of valid rows processed: " + numberOfValidRows);
        Log.d(TAG, "Total # of rows skipped: " + numberOfSkippedRows);

    }

    private void showToast(String msg) {


    }

}



