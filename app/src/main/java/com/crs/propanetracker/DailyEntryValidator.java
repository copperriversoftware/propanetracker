package com.crs.propanetracker;

import android.database.Cursor;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DailyEntryValidator {
    private static final String TAG = DailyEntryValidator.class.getSimpleName();
    public static final String VALID_ENTRY = "Entry successfully recorded.";
    private static final SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

    public static String validateEntry(Cursor cursor, DailyEntry newEntry) {
        String returnValue = "False";

        cursor.moveToFirst();
        Log.d(TAG, "Validating new entry with date: " + newEntry.getDate() + " & Level: " + newEntry.getLevel());
        Log.d(TAG, "Validating against cursor with " + cursor.getCount() + " entries");

        int newLevel = newEntry.getLevel();
        Date newDate = newEntry.getDate();

        //Validate not a future date
        Date currentDate = Calendar.getInstance().getTime();
        if (newDate.after(currentDate)) {
            return "Entries cannot be in the future.";
        }

        switch (cursor.getCount()) {
            case 0:
                //This is the first entry, so no need to validate
                Log.d(TAG, "This is the first entry, so no need to validate.");

                //Everything passed, so lets explicitly set valid to true now.
                returnValue = VALID_ENTRY;
                break;
            case 1:
                //This case occurs when the query returns one entry.  Comments in individual IF/ELSE statements below
                DailyEntry entryFromCursor = DailyEntryDAO.cursorToDailyEntry(cursor);
                Date cursorDate = entryFromCursor.getDate();
                int cursorLevel = entryFromCursor.getLevel();
                if (newDate.before(cursorDate)) {
                    //This is going to result in adding the earliest chronological entry in the DB
                    if (newLevel < cursorLevel) {
                        returnValue = "New level of (" + newLevel + ") must be greater than the level of the following " +
                                "chronological entry of (" + cursorLevel + ") on date " + cursorDate;
                        Log.d(TAG, returnValue);
                        return returnValue;
                    }
                } else if (newDate.after(cursorDate)) {
                    //This is going to result in adding the latest chronological entry in the DB
                    if (newLevel > cursorLevel) {
                        returnValue = "New level of (" + newLevel + ") must be less than the level of the previous " +
                                "chronological entry of (" + cursorLevel + ") on date " + cursorDate;
                        Log.d(TAG, returnValue);
                        return returnValue;
                    }
                } else {
                    //Entry is the same day
                    if (newDate.getDay() == cursorDate.getDay() && newDate.getMonth() == cursorDate.getMonth() && newDate.getYear() == cursorDate.getYear()) {
                        if (newLevel >= cursorLevel) {
                            returnValue = "New level of (" + newLevel + ") must be less than the level of the previous " +
                                    "chronological entry of (" + cursorLevel + ") on date " + cursorDate;
                            Log.d(TAG, returnValue);
                            return returnValue;
                        }
                    } else {
                        returnValue = "Error. Something went wrong. Unable to save entry.";
                        Log.d(TAG, returnValue);
                        return returnValue;
                    }
                }

                //Everything passed, so lets explicitly set valid to true now.
                returnValue = VALID_ENTRY;
                break;
            case 2:
                //There are existing entries in the DB before and after this new entry
                DailyEntry olderEntry = DailyEntryDAO.cursorToDailyEntry(cursor);
                Date olderDate = olderEntry.getDate();
                int olderLevel = olderEntry.getLevel();

                cursor.moveToNext();

                DailyEntry newerEntry = DailyEntryDAO.cursorToDailyEntry(cursor);
                Date newerDate = newerEntry.getDate();
                int newerLevel = newerEntry.getLevel();

                Log.d(TAG, "new entry is after fillup: " + newEntry.isEntryAfterFillUp());


                if (newDate.after(olderDate) && newerDate.compareTo(olderDate) == 0) {
                    //One off case to handle the first new Daily Entry after a fillup, because there will be two entries
                    //on the same date after a fillup.  The value before fillup and the value after fillup.  This case causes
                    //the check to fail because typically a new entry should be between the newer and older entries.
                    return VALID_ENTRY;
                }

                if (newDate.before(olderDate) || newDate.after(newerDate)) {
                    //New entry should be between older and newer date
                    returnValue = "Error: New entry date (" + df.format(
                            newDate) + ") should be between older (" + df.format(olderDate) + ") " +
                            "and newer date (" + df.format(newerDate) + ")";
                    Log.d(TAG, returnValue);
                    return returnValue;
                }

                if (newLevel >= olderLevel) {
                    returnValue = "New level of (" + newLevel + ") must be less than the level of the previous " +
                            "chronological entry of (" + olderLevel + ") on date " + df.format(olderDate);
                    Log.d(TAG, returnValue);
                    return returnValue;
                }

                if (newLevel <= newerLevel) {
                    returnValue = "New level of (" + newLevel + ") must be greater than the level of the following " +
                            "chronological entry of (" + newerLevel + ") on date " + df.format(newerDate);
                    Log.d(TAG, returnValue);
                    return returnValue;
                }

                //Everything passed, so lets explicitly set valid to true now.
                returnValue = VALID_ENTRY;
                break;


        }

        //Put things back like we got em.
        cursor.moveToFirst();
        return returnValue;
    }
}
