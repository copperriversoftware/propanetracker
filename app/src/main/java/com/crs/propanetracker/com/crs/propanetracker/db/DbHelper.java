package com.crs.propanetracker.com.crs.propanetracker.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {
    private static final String TAG = DbHelper.class.getSimpleName();
    //    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    //    private final Context mCtx;
    static final int DATABASE_VERSION = 1;

    private static DbHelper dbHelper;

    private static final String DATABASE_NAME = "propaneTracker.db";

    public static final String TABLE_HISTORY = "history";
    public static final String COLUMN_ID = "_ID";
    public static final String COLUMN_LEVEL = "level";
    public static final String COLUMN_NOTES = "notes";
    public static final String COLUMN_DATE = "entryDate";

    public static final String TABLE_FILL_UPS = "fillups";
    public static final String COLUMN_VOLUME = "volume";
    public static final String COLUMN_PRICE = "price";


    /**
     * Database sql statements
     */
    //@formatter:off
    static final String DATABASE_HISTORY_TABLE_CREATE =
            "CREATE TABLE " + TABLE_HISTORY + "(_id INTEGER primary key autoincrement, " +
                    COLUMN_LEVEL + " INTEGER(3) not null," +
                    COLUMN_NOTES + " TEXT," +
                    COLUMN_DATE + "  INTEGER not null);";

    static final String DATABASE_FILL_UPS_TABLE_CREATE =
            "CREATE TABLE " + TABLE_FILL_UPS + "(_id INTEGER primary key autoincrement, " +
                    COLUMN_LEVEL + " INTEGER(3) not null," +
                    COLUMN_VOLUME + " FLOAT," +
                    COLUMN_PRICE + " FLOAT," +
                    COLUMN_NOTES + " TEXT," +
                    COLUMN_DATE + " INTEGER not null);";
    //@formatter:on

    public static final String[] HISTORY_TABLE_ALL_COLUMNS = {DbHelper.COLUMN_ID, DbHelper.COLUMN_DATE, DbHelper.COLUMN_LEVEL, DbHelper.COLUMN_NOTES};

    private DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "Constructing the DB Adapter");
    }

    public static DbHelper getInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new DbHelper(context);
        }

        return dbHelper;
    }

    public void runTestQuery() {
        SQLiteDatabase db = getWritableDatabase();

        String selection = ("SELECT * FROM history WHERE date IN\n" +
                "    (SELECT max(date)\n" +
                "    FROM employees\n" +
                "    WHERE date < \"2018-07-12\"\n" +
                "    UNION\n" +
                "    SELECT min(date)\n" +
                "    FROM employees\n" +
                "    WHERE date > \"2018-07-12\")\n" +
                "    ORDER BY  date(date)\n");

        Cursor cursor = db.query(DbHelper.TABLE_FILL_UPS, HISTORY_TABLE_ALL_COLUMNS, null, null, null, null, null);
        Log.d(TAG, "cursor: " + cursor);
    }

    public static String generateDailyEntrySelectionClause(long time) {

        return "entryDate IN" +
                "    (SELECT max(entryDate)" +
                "    FROM history" +
                "    WHERE entryDate <=" + time +
                "    UNION" +
                "    SELECT min(entryDate)" +
                "    FROM history" +
                "    WHERE entryDate >=" + time + ")";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "Executing sql: " + DATABASE_HISTORY_TABLE_CREATE);
        db.execSQL(DATABASE_HISTORY_TABLE_CREATE);
        Log.d(TAG, "Created table: " + TABLE_HISTORY);

        Log.d(TAG, "Executing sql: " + DATABASE_FILL_UPS_TABLE_CREATE);
        db.execSQL(DATABASE_FILL_UPS_TABLE_CREATE);
        Log.d(TAG, "Created table: " + TABLE_FILL_UPS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);
    }

    public void deleteAllDailyEntries() {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d(TAG, "Deleting all Daily Entries from the database");

        //        List<DailyEntry> dailyEntries = getAllDailyEntries();
        //        Log.d(TAG, "Number of entries in the table [" + DbHelper.TABLE_HISTORY + "] is: " + dailyEntries.size());
        db.execSQL("delete from " + DbHelper.TABLE_HISTORY);
        //        dailyEntries = getAllDailyEntries();
        //        Log.d(TAG, "After Delete All number of entries in the table [" + DbHelper.TABLE_HISTORY + "] is: " + dailyEntries.size());
    }
}
