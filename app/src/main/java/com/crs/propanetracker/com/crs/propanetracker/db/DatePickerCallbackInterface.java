package com.crs.propanetracker.com.crs.propanetracker.db;

/**
 * Created by cbeaudin on 5/14/2015.
 */
public interface DatePickerCallbackInterface {

    public void setDate(int year, int month, int day);

}
