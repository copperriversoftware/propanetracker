package com.crs.propanetracker.com.crs.propanetracker.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by ninja on 1/26/17.
 */

public class PropaneContentProvider extends ContentProvider {

    private static final String TAG = "PropaneCP";

    public static final String PROVIDER_NAME = "com.crs.propanetracker.db.PropaneContentProvider";

    private DbHelper dbHelper;

    // Holds the database object
    private SQLiteDatabase db;

    //URI Matches
    private static final int TABLE_HISTORY = 1;
    private static final int TABLE_HISTORY_RECORD = 2;
    private static final int TABLE_FILL_UP = 3;
    private static final int TABLE_FILL_UP_RECORD = 4;

    // Creates a UriMatcher object.
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
    /*
     * The calls to addURI() go here, for all of the content URI patterns that the provider
     * should recognize. For this snippet, only the calls for table 3 are shown.
     */

    /*
     * Sets the integer value for multiple rows in table 3 to 1. Notice that no wildcard is used
     * in the path
     */
        sUriMatcher.addURI(PROVIDER_NAME, DbHelper.TABLE_HISTORY, 1);
        sUriMatcher.addURI(PROVIDER_NAME, DbHelper.TABLE_HISTORY+"/#", 2);
        sUriMatcher.addURI(PROVIDER_NAME, DbHelper.TABLE_FILL_UPS, 3);
        sUriMatcher.addURI(PROVIDER_NAME, DbHelper.TABLE_FILL_UPS+"/#", 4);
    }

    @Override
    public boolean onCreate() {

        /*
         * Creates a new helper object. This method always returns quickly.
         * Notice that the database itself isn't created or opened
         * until SQLiteOpenHelper.getWritableDatabase is called
         */
        dbHelper = DbHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Log.d(TAG, "In query method...");

        String tableName = null;

        /*
         * Choose the table to query and a sort order based on the code returned for the incoming
         * URI. Here, too, only the statements for table 3 are shown.
         */
        switch (sUriMatcher.match(uri)) {


            // If the incoming URI was for all of table3
            case TABLE_HISTORY:
                tableName = DbHelper.TABLE_HISTORY;
                if (TextUtils.isEmpty(sortOrder)) sortOrder = " _ID ASC";
                break;
            case TABLE_FILL_UP:
                tableName = DbHelper.TABLE_FILL_UPS;
                if (TextUtils.isEmpty(sortOrder)) sortOrder = " _ID ASC";
                break;

            // ***
            /*
             The cases below require more logic to work correctly. There's no guarantee the
             user won't pass in null for selection - although it would be odd given the
             URI required to get here in the first place.
             */
            //***
            // If the incoming URI was for a single row
            case TABLE_HISTORY_RECORD:
                tableName = DbHelper.TABLE_HISTORY;
                /*
                 * Because this URI was for a single row, the _ID value part is
                 * present. Get the last path segment from the URI; this is the _ID value.
                 * Then, append the value to the WHERE clause for the query
                 */
                if (selection == null) {
                    selection = "_ID = " + uri.getLastPathSegment();
                } else {
                    selection = selection + " _ID = " + uri.getLastPathSegment();
                }
                break;
            case TABLE_FILL_UP_RECORD:
                tableName = DbHelper.TABLE_FILL_UPS;
                /*
                 * Because this URI was for a single row, the _ID value part is
                 * present. Get the last path segment from the URI; this is the _ID value.
                 * Then, append the value to the WHERE clause for the query
                 */
                selection = selection + " _ID = " + uri.getLastPathSegment();
                break;

            default:
                // If the URI is not recognized, you should do some error handling here.
        }

        // call the code to actually do the query

        Log.d(TAG, "Matched "+tableName);
        /*
         * Gets a writeable database. This will trigger its creation if it doesn't already exist.
         *
         */
        if (db == null || db.isReadOnly()) {
            db = dbHelper.getWritableDatabase();
        }

        return db.query(tableName,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {


            // If the incoming URI was for all of a table
            case 1:
            case 3:
                return "vnd.android.cursor.dir/vnd."+PROVIDER_NAME+ uri.getLastPathSegment();

            // If the incoming URI was for a single row
            case 2:
            case 4:
                return "vnd.android.cursor.item/vnd."+PROVIDER_NAME+ uri.getLastPathSegment();

            default:
                return null;
                // If the URI is not recognized, you should do some error handling here.
        }

    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long recNum = -1;
        String tableName = "";

        /*
         * Gets a writeable database. This will trigger its creation if it doesn't already exist.
         *
         */
        db = dbHelper.getWritableDatabase();

        switch (sUriMatcher.match(uri)) {
            case TABLE_HISTORY:
                tableName = DbHelper.TABLE_HISTORY;
                recNum = db.insert(DbHelper.TABLE_HISTORY, null, values);
                break;
            case TABLE_FILL_UP:
                tableName = DbHelper.TABLE_FILL_UPS;
                recNum = db.insert(DbHelper.TABLE_FILL_UPS, null, values);
                break;
            default:
                break;
        }

        String path = "content://"+PROVIDER_NAME+"/"+tableName+"/"+recNum;

        return Uri.parse(path);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        /*
         * Gets a writeable database. This will trigger its creation if it doesn't already exist.
         *
         */
        db = dbHelper.getWritableDatabase();

        switch (sUriMatcher.match(uri)) {
            case TABLE_HISTORY:
                return db.delete(DbHelper.TABLE_HISTORY, selection, selectionArgs);
            case TABLE_FILL_UP:
                return db.delete(DbHelper.TABLE_FILL_UPS, selection, selectionArgs);
            default:
                return 0;
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        /*
         * Gets a writeable database. This will trigger its creation if it doesn't already exist.
         *
         */
        db = dbHelper.getWritableDatabase();

        switch (sUriMatcher.match(uri)) {
            case TABLE_HISTORY:
                return db.update(DbHelper.TABLE_HISTORY, values, selection, selectionArgs);
            case TABLE_FILL_UP:
                return db.update(DbHelper.TABLE_FILL_UPS, values, selection, selectionArgs);
            default:
                return 0;
        }
    }
}
