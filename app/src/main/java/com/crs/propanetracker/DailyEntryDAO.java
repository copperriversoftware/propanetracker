package com.crs.propanetracker;

import android.annotation.SuppressLint;
import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.crs.propanetracker.com.crs.propanetracker.db.DbHelper;
import com.crs.propanetracker.com.crs.propanetracker.db.PropaneContentProvider;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DailyEntryDAO extends AsyncQueryHandler {
    private static final String TAG = DailyEntryDAO.class.getSimpleName();

    private final String url = "content://" + PropaneContentProvider.PROVIDER_NAME + "/" + DbHelper.TABLE_HISTORY;

    public static final int NEW_DAILY_ENTRY = 2;
    public static final int INSERT_ONE = 3;
    public static final int GENERAL_QUERY = 10;
    public static final int FROM_CREATE_STEP_1 = 12;
    public static final int FROM_CREATE_STEP_2 = 13;
    public static final int GET_ALL_ENTRIES = 14;
    public static final int GET_ALL_ENTRIES_NEWEST_FIRST = 15;
    public static final int DELETE_ALL_ENTRIES = 16;

    private Context context;

    public static final String URL = "content://" + PropaneContentProvider.PROVIDER_NAME + "/" + DbHelper.TABLE_HISTORY;

    //projections
    public static final String[] ALL_COLUMNS = {DbHelper.COLUMN_ID, DbHelper.COLUMN_DATE, DbHelper.COLUMN_LEVEL, DbHelper.COLUMN_NOTES};
    private static final String[] ALL_COLUMNS_MAX_DATE = {DbHelper.COLUMN_ID, "MAX(" + DbHelper.COLUMN_DATE + ") AS " + DbHelper.COLUMN_DATE, DbHelper.COLUMN_LEVEL, DbHelper.COLUMN_NOTES};

    //query component
    private static final String SELECT_MAX_LEVEL = DbHelper.COLUMN_LEVEL + " = (SELECT MAX(" + DbHelper.COLUMN_LEVEL + ") FROM " + DbHelper.TABLE_HISTORY + ")";


    private List<DailyEntry> dailyEntries;

    public DailyEntryDAO(Context context) {
        super(context.getContentResolver());
        this.context = context;
    }


    void createDailyEntriesFromFillup(FillUp fillUp) {
        startQuery(FROM_CREATE_STEP_1, fillUp, Uri.parse(URL), ALL_COLUMNS_MAX_DATE, SELECT_MAX_LEVEL, null, null);
    }

    private boolean validateDate(FillUp fillUp) {
        //Validate not a future date
        Date currentDate = Calendar.getInstance().getTime();
        if (fillUp.getDate().after(currentDate)) {
            showToast("Cannot add future fill-ups.");
            return false;
        }
        return true;
    }

    private boolean validateLevel(FillUp fillUp, int level, Cursor c) {
        c.moveToFirst();
        if (!c.isAfterLast()) {
            DailyEntry lastRecordedDailyEntry = cursorToDailyEntry(c);
            if (checkLevelIsLessThanPrevious(fillUp.getLevelBeforeFillUp(), lastRecordedDailyEntry.getLevel())) {
                showToast(
                        "Level before fill-up (" + fillUp.getLevelBeforeFillUp() + ") should be less than last recorded entry(" + lastRecordedDailyEntry.getLevel() + ").");
                return false;
            }
        }

        return true;
    }

    static synchronized boolean checkLevelIsLessThanPrevious(int levelBeforeFillUp, int lastRecordedlevel) {
        boolean isLess = false;
        Log.d(TAG, "levelBeforeFillUp " + levelBeforeFillUp);
        Log.d(TAG, "lastRecordedlevel " + lastRecordedlevel);
        //If the user enters a fillup first, there will be no recorded previous level.  If the last recorded value is 0, you can't ever enter a level less.
        if (lastRecordedlevel == 0) {
            return isLess;
        }
        //Check that the new entry has a lower fill level than the last entry
        if (lastRecordedlevel < levelBeforeFillUp) {
            Log.d(TAG, "Validation error! Level before fill-up should be less than last recorded entry.");
            isLess = true;
        }
        return isLess;
    }


    private void printAllEntries(Cursor c) {
        c.moveToFirst();

        Log.d(TAG,
                "-------  Printing all (" + c.getCount() + ") Daily Entries in chronological order (oldest -> newest)  -------");
        while (!c.isAfterLast()) {
            DailyEntry dailyEntry = DailyEntryDAO.cursorToDailyEntry(c);
            Log.d(TAG, dailyEntry.toString());
            c.moveToNext();
        }

        Log.d(TAG, "----------   Finished Printing Daily Entries .     ----------");

        c.moveToFirst();
    }

    synchronized static DailyEntry cursorToDailyEntry(Cursor cursor) {
//        Log.d(TAG, "cursor.getCount: " + cursor.getCount());
//        Log.d(TAG, "cursor.getColumnCount: " + cursor.getColumnCount());
//        Log.d(TAG, "cursor.getLong: " + cursor.getLong(0));
        DailyEntry dailyEntry = new DailyEntry();
        int level = cursor.getColumnIndex("level");
        int notes = cursor.getColumnIndex("notes");
        int id = cursor.getColumnIndex("_id");

        dailyEntry.setLevel(cursor.getInt(2));
        dailyEntry.setNotes(cursor.getString(3));

        dailyEntry.setId(cursor.getLong(0));
        Date date = new Date(cursor.getLong(1));
        dailyEntry.setDate(date);
        Log.d(TAG,
                "in cursorToDailyEntry.  Date:" + dailyEntry.getDateAsText() + " level:" + dailyEntry.getLevel() +
                        " notes: " + dailyEntry.getNotes());

        return dailyEntry;
    }

    private void showToast(String msg) {
        if (msg.equalsIgnoreCase(DailyEntryValidator.VALID_ENTRY)) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }


    private void insertEntry(DailyEntry newEntry) {
        ContentValues values = new ContentValues();
        values.put(DbHelper.COLUMN_DATE, newEntry.getDate().getTime());
        values.put(DbHelper.COLUMN_LEVEL, newEntry.getLevel());
        values.put(DbHelper.COLUMN_NOTES, newEntry.getNotes());

        Log.d(TAG, "Calling startInsert for new Daily Entry: " + newEntry.toString());
        startInsert(INSERT_ONE, newEntry, Uri.parse(url), values);
    }

    private void getAllDailyEntries() {
        startQuery(GET_ALL_ENTRIES, null, Uri.parse(URL), ALL_COLUMNS, null, null, DbHelper.COLUMN_DATE);
    }

    private List<DailyEntry> getAllDailyEntries(Cursor cursor) {
        dailyEntries = new ArrayList<DailyEntry>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            DailyEntry dailyEntry = cursorToDailyEntry(cursor);
            dailyEntries.add(dailyEntry);
            cursor.moveToNext();
        }

        cursor.close();
        Log.d(TAG, "Number of DailyEntries: " + dailyEntries.size());
        return dailyEntries;
    }

    private void getAllDailyEntriesNewestFirst() {
        startQuery(GET_ALL_ENTRIES_NEWEST_FIRST, ALL_COLUMNS, null, null, null, null, DbHelper.COLUMN_DATE + " DESC");
    }

    private List<DailyEntry> getAllDailyEntriesNewestFirst(Cursor cursor) {
        dailyEntries = new ArrayList<>();

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            DailyEntry dailyEntry = cursorToDailyEntry(cursor);
            dailyEntries.add(dailyEntry);
            cursor.moveToNext();
        }

        cursor.moveToFirst();
        Log.d(TAG, "Number of DailyEntries: " + dailyEntries.size());
        return dailyEntries;
    }

    private void deleteEntry(DailyEntry dailyEntry) {
//        long id = dailyEntry.getId();
//        Log.d(TAG, "Deleted entry with id: " + id);
//        int numRowsDeleted = database.delete(DbHelper.TABLE_HISTORY, DbHelper.COLUMN_ID + " = " + id, null);
//        if (numRowsDeleted == 1) {
//            Log.d(TAG, "Successfully deleted " + numRowsDeleted + "row");
//        } else {
//            Log.e(TAG, "Something went wrong.  Instead of deleting one row we deleted: " + numRowsDeleted);
//        }
    }

    @SuppressLint("HandlerLeak")
    @NonNull
    public AsyncQueryHandler getAsyncQueryHandler() {
        return new AsyncQueryHandler(context.getContentResolver()) {
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                ContentValues values;
                Log.d(TAG, "onQueryComplete completed. Token value: " + token);
                Log.d(TAG, "Is cursor last: " + cursor.isLast());

                switch (token) {
                    case GENERAL_QUERY:
                        break;
                    case GET_ALL_ENTRIES:
                        getAllDailyEntries(cursor);
                        break;
                    case GET_ALL_ENTRIES_NEWEST_FIRST:
                        getAllDailyEntriesNewestFirst(cursor);
                        break;
                    case NEW_DAILY_ENTRY:
                        DailyEntry newEntry = ((DailyEntry) cookie);
                        insertDailyEntryIfValid(cursor, newEntry);
                        cursor.close();
                        break;
                    case FROM_CREATE_STEP_1:
                        FillUp fillUp = ((FillUp) cookie);
                        if (validateDate(fillUp) && validateLevel(fillUp, fillUp.getLevelBeforeFillUp(), cursor)) {

                            Log.d(TAG, "Adding new entry to DB with the following information");
                            Log.d(TAG, "date: " + fillUp.getDate().toString());
                            Log.d(TAG, "level: " + fillUp.getLevelBeforeFillUp());
                            Log.d(TAG, "notes: " + fillUp.getNotes());

                            values = new ContentValues();
                            values.put(DbHelper.COLUMN_DATE, fillUp.getDate().getTime());
                            values.put(DbHelper.COLUMN_LEVEL, fillUp.getLevelBeforeFillUp());
                            values.put(DbHelper.COLUMN_NOTES, fillUp.getNotes());

                            startInsert(FROM_CREATE_STEP_1, fillUp, Uri.parse(URL), values);
                        }
                        break;
                    case FROM_CREATE_STEP_2:
                        FillUp fillUp2 = ((FillUp) cookie);
                        if (validateDate(fillUp2)) {
                            Log.d(TAG, "Adding new entry to DB with the following information");
                            Log.d(TAG, "date: " + fillUp2.getDate().toString());
                            Log.d(TAG, "level: " + fillUp2.getLevelAfterFillUp());
                            Log.d(TAG, "notes: " + fillUp2.getNotes());

                            values = new ContentValues();
                            values.put(DbHelper.COLUMN_DATE, fillUp2.getDate().getTime());
                            values.put(DbHelper.COLUMN_LEVEL, fillUp2.getLevelAfterFillUp());
                            values.put(DbHelper.COLUMN_NOTES, fillUp2.getNotes());

                            startInsert(FROM_CREATE_STEP_2, fillUp2, Uri.parse(URL), values);
                        }
                        break;
                }
                cursor.close();
            }

            @Override
            protected void onInsertComplete(int token, Object cookie, Uri uri) {
                Log.d(TAG, "Insert complete with return token/code: " + token);
                switch (token) {
                    case INSERT_ONE:
                        DailyEntry dailyEntry = ((DailyEntry) cookie);
                        Log.d(TAG, "Finished inserting new DailyEntry: " + dailyEntry.getDate());
                        break;
                    case FROM_CREATE_STEP_1:
                        Log.d(TAG, "finished inserting 1'st record.");
                        FillUp fillUp = ((FillUp) cookie);
                        // we pass in null this time for selection because the uri returned here is the actual record.
                        startQuery(FROM_CREATE_STEP_2, fillUp, uri, ALL_COLUMNS_MAX_DATE, null, null, null);
                        break;
                    case FROM_CREATE_STEP_2:
                        FillUp fillUp2 = ((FillUp) cookie);
                        Log.d(TAG, "finished inserting 2'nd record.");
                        Intent intent = new Intent(FillUpDAO.DAILY_ENTRIES_SUCCESS);
                        intent.putExtra("fillUp", fillUp2);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        break;
                }
            }

            @Override
            protected void onDeleteComplete(int token, Object cookie, int result) {
                showToast("Deleted all entries");
            }
        };
    }

    /**
     * @param cursor
     * @param newEntry
     */
    public void insertDailyEntryIfValid(Cursor cursor, DailyEntry newEntry) {

        if (cursor == null || cursor.getCount() == 0) {
            //First entry in the DB, so we don't need to validate this newEntry.  Just insert it.
            insertEntry(newEntry);
        } else {

            Log.d(TAG, "Entry After Fillup??? " + newEntry.isEntryAfterFillUp());

            if (newEntry.isEntryAfterFillUp()) {
                insertEntry(newEntry);
                showToast("Entry after fillup successfully recorded.");
            } else {
                String isValidEntry = DailyEntryValidator.validateEntry(cursor, newEntry);
                if (isValidEntry.equalsIgnoreCase(DailyEntryValidator.VALID_ENTRY)) {
                    insertEntry(newEntry);
                }
                showToast(isValidEntry);
            }

        }
    }
}
