package com.crs.propanetracker;


import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, DailyEntryFragment.OnFragmentInteractionListener, FillUpFragment.OnFragmentInteractionListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int PERMISSION_REQUEST_CODE = 1;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    public static final String FRAG_DAILY = "Daily";
    private static final String FRAG_FILLUP = "Fillup";
    private static final String FRAG_CHARTS = "Charts";
    private static final String FRAG_SETTINGS = "Settings";

    private static final String CURRENT_FRAG = "current_frag";

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermissions();

        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(
                R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        initFrags(savedInstanceState);
    }

    private void checkPermissions() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                Log.d(TAG, "Permission denied to read external storage.  Requesting it now");
                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};

                requestPermissions(permissions, PERMISSION_REQUEST_CODE);

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Log.d(TAG, "User denied request for file read permission.  Show explanation");
                    //TODO Show explanation of why they should accept this permission.

                }

            }
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        initFrags(savedInstanceState);
    }

    // invoked when the activity may be temporarily destroyed, save the instance state here
    @Override
    public void onSaveInstanceState(Bundle outState) {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        String fragName = backStackEntryCount > 0 ? getSupportFragmentManager().getBackStackEntryAt(
                backStackEntryCount - 1).getName() : FRAG_DAILY;
        outState.putString(CURRENT_FRAG, fragName);
        //getSupportFragmentManager().popBackStackImmediate();
        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    private void initFrags(Bundle savedInstanceState) {
        String fragName = null;
        Fragment frag = null;

        if (savedInstanceState != null) {
            Log.d(TAG, "SavedInstanceState != null");
            fragName = savedInstanceState.getString(CURRENT_FRAG);
            frag = getSupportFragmentManager().findFragmentByTag(fragName);

            Log.d(TAG,
                    "SavedInstanceState - fragName: " + fragName + " frag == " + (frag == null ? null : frag.getTag()));
        }

        FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
        if (frag != null) {
            Log.d(TAG, "SavedInstanceState - frag != null");
            //trans.addToBackStack(fragName);
            trans.replace(R.id.container, frag, fragName)
                    .commit();
        } else {
            Log.d(TAG, "SavedInstanceState - frag == null");
            frag = DailyEntryFragment.newInstance();
            trans.addToBackStack(FRAG_DAILY);
            trans.replace(R.id.container, frag, FRAG_DAILY)
                    .commit();
        }

        mTitle = frag.getTag();
        getSupportFragmentManager().executePendingTransactions();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 2) {
            Log.d(TAG, "Backstack Entry Count: " + getSupportFragmentManager().getBackStackEntryCount());
            finish();
        } else {
            FragmentManager fm = getSupportFragmentManager();
            fm.popBackStack();
            if (fm.getBackStackEntryCount() > 1) {
                mTitle = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 2).getName();
                restoreActionBar();
            }
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        //FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment frag;
        if (position == 0) {
            // Show main screen
            frag = (DailyEntryFragment) getSupportFragmentManager().findFragmentByTag(FRAG_DAILY);
            if (frag == null) {
                frag = DailyEntryFragment.newInstance();
                fragmentTransaction.addToBackStack(FRAG_DAILY);
                fragmentTransaction.replace(R.id.container, frag, FRAG_DAILY);
            } else {
                getSupportFragmentManager().popBackStack(FRAG_DAILY, 0);
            }

            mTitle = getString(R.string.daily_entry);
        } else if (position == 1) {
            // Selected the Fill Up screen
            frag = (FillUpFragment) getSupportFragmentManager().findFragmentByTag(FRAG_FILLUP);
            if (frag == null) {
                Log.d(TAG, "Create a new instance of FillUp Fragment.");
                frag = FillUpFragment.newInstance("arg0", "arg1");
                fragmentTransaction.addToBackStack(FRAG_FILLUP);
                fragmentTransaction.replace(R.id.container, frag, FRAG_FILLUP);
            } else {
                getSupportFragmentManager().popBackStack(FRAG_FILLUP, 0);
            }

            mTitle = getString(R.string.fill_up);
        } else if (position == 2) {
            // Selected the Charts screen

            frag = (ChartsFragment) getSupportFragmentManager().findFragmentByTag(FRAG_CHARTS);
            if (frag == null) {
                Log.d(TAG, "Create a new instance of Charts Fragment.");
                frag = ChartsFragment.newInstance();
                fragmentTransaction.addToBackStack(FRAG_CHARTS);
                fragmentTransaction.replace(R.id.container, frag, FRAG_CHARTS);
            } else {
                getSupportFragmentManager().popBackStack(FRAG_CHARTS, 0);
            }

            mTitle = getString(R.string.charts);
        } else if (position == 3) {
            //Selected the Settings screen

            frag = (SettingsFragment) getSupportFragmentManager().findFragmentByTag(FRAG_SETTINGS);
            if (frag == null) {
                Log.d(TAG, "Create a new instance of Settings Fragment");
                frag = SettingsFragment.newInstance();
                fragmentTransaction.addToBackStack(FRAG_SETTINGS);
                fragmentTransaction.replace(R.id.container, frag, FRAG_SETTINGS);
            } else {
                getSupportFragmentManager().popBackStack(FRAG_SETTINGS, 0);

            }

            mTitle = getString(R.string.settings);
        }

        fragmentTransaction.commit();
        getSupportFragmentManager().executePendingTransactions();

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.main_screen);
                break;
            case 2:
                mTitle = getString(R.string.fill_up);
                break;
            case 3:
                mTitle = getString(R.string.charts);
                break;
            case 4:
                mTitle = getString(R.string.settings);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        DrawerLayout sideBar = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle sideBarToggle = new ActionBarDrawerToggle(this, sideBar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        sideBar.addDrawerListener(sideBarToggle);
        sideBarToggle.syncState();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Log.d(TAG, "Callback from DailyEntryFragment");
    }


    //    /**
    //     * A placeholder fragment containing a simple view.
    //     */
    //    public static class PlaceholderFragment extends Fragment {
    //        /**
    //         * The fragment argument representing the section number for this
    //         * fragment.
    //         */
    //        private static final String ARG_SECTION_NUMBER = "section_number";
    //
    //        /**
    //         * Returns a new instance of this fragment for the given section
    //         * number.
    //         */
    //        public static PlaceholderFragment newInstance(int sectionNumber) {
    //            PlaceholderFragment fragment = new PlaceholderFragment();
    //            Bundle args = new Bundle();
    //            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
    //            fragment.setArguments(args);
    //            return fragment;
    //        }
    //
    //        public PlaceholderFragment() {
    //        }
    //
    ////        @Override
    ////        public View onCreateView(LayoutInflater inflater, ViewGroup container,
    ////                                 Bundle savedInstanceState) {
    ////            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
    ////            return rootView;
    ////        }
    //
    //        @Override
    //        public void onAttach(Activity activity) {
    //            super.onAttach(activity);
    //            ((MainActivity) activity).onSectionAttached(
    //                    getArguments().getInt(ARG_SECTION_NUMBER));
    //        }
    //    }

}
