package com.crs.propanetracker;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.crs.propanetracker.com.crs.propanetracker.db.DbHelper;
import com.crs.propanetracker.com.crs.propanetracker.db.PropaneContentProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * When recording a fill-up we also have to record a daily entry value.
 * We need to know the value of the tank before the fill-up and the value after the fill-up.
 * The value before the fill-up is used to make an entry in the DailyEntry table.
 */
public class FillUpFragment extends RootFragment {

    private static final String TAG = FillUpFragment.class.getSimpleName();


    private final String url = "content://" + PropaneContentProvider.PROVIDER_NAME + "/" + DbHelper.TABLE_FILL_UPS;

    private FillUpDAO fillUpDAO;
    private DailyEntryDAO dailyEntryDAO;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private final SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    /**
     * Use this factory method to create a new instance of this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DailyEntryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FillUpFragment newInstance(String param1, String param2) {
        FillUpFragment fragment = new FillUpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FillUpFragment() {
        //Required empty constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            Log.d(TAG, "arg0: " + mParam1);
            Log.d(TAG, "arg1: " + mParam2);
        }

        dailyEntryDAO = new DailyEntryDAO(this.getActivity().getApplicationContext());
        // dailyEntryDAO.open();

        fillUpDAO = new FillUpDAO(getActivity().getApplicationContext());
        //fillUpDAO.open();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "Detached.");
        mListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_fillup, container, false);
        final TextView dateValue = (TextView) rootView.findViewById(R.id.fillUpDateValue);
        final TextView levelBeforeFillValue = (TextView) rootView.findViewById(R.id.levelBeforeFillValue);
        final TextView levelAfterFillValue = (TextView) rootView.findViewById(R.id.levelAfterFillValue);
        final TextView gallonsValue = (TextView) rootView.findViewById(R.id.fillUpVolumeValue);
        final TextView priceValue = (TextView) rootView.findViewById(R.id.priceValue);
        final TextView notesValue = (TextView) rootView.findViewById(R.id.fillUpNotesValue);

        FillUp fillUp = new FillUp();
        dateValue.setText(fillUp.getDateAsText());
        dateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment selectDateFragment = new SelectDateFragment();
                selectDateFragment.setTargetFragment(FillUpFragment.this, 0);
                selectDateFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        rootView.findViewById(R.id.saveFillUpButton).

                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "Save Fill Up button pressed");

                        Log.d(TAG, "date: " + dateValue.getText());
                        Log.d(TAG, "Tank level before Fill: " + levelBeforeFillValue.getText().toString());
                        Log.d(TAG, "Tank level after Fill: " + levelAfterFillValue.getText().toString());

                        DailyEntryFragment dailyEntryFragment = (DailyEntryFragment) getFragmentManager().findFragmentByTag(
                                MainActivity.FRAG_DAILY);
                        Date date = null;
                        try {
                            date = df.parse(dateValue.getText().toString());
                        } catch (ParseException e) {
                            Log.e(TAG, "Error parsing date", e);
                        }

                        /**
                         //FIXME.  Need to handle these failing validation. As it stands, the Fill-Up process will continue and the Daily Entries on either side will not be inserted. (Not ideal)
                         //Make DailyEntry for the values before the fillup
                         DailyEntry dailyEntryBeforeFillup = new DailyEntry();
                         dailyEntryBeforeFillup.setLevel(Integer.valueOf(levelBeforeFillValue.getText().toString()));
                         dailyEntryBeforeFillup.setDate(date);
                         dailyEntryFragment.startAsyncQueryForDailyEntry(dailyEntryBeforeFillup);

                         //Make DailyEntry for the values before the fillup
                         DailyEntry dailyEntryAfterFillup = new DailyEntry();
                         dailyEntryAfterFillup.setLevel(Integer.valueOf(levelAfterFillValue.getText().toString()));
                         dailyEntryAfterFillup.setDate(date);
                         //Must set this so validation will pass
                         dailyEntryAfterFillup.setIsEntryAfterFillUp(true);
                         dailyEntryFragment.startAsyncQueryForDailyEntry(dailyEntryAfterFillup);
                         */

                        FillUp fillUp = createFillUpFromView();

//                        //Make entry for FillUp
//                        String fillUpValidEntry = fillUpDAO.createFillUp(fillUp, dailyEntryDAO, getContext());
//                        displayToast(fillUpValidEntry);
//                        fillUpDAO.createFillUp(fillUp, dailyEntryDAO, getContext());

                        if (fillUp != null) {
                            long time = fillUp.getDate().getTime();
                            String selectionClause = "entryDate IN" +
                                    "    (SELECT max(entryDate)" +
                                    "    FROM fillups" +
                                    "    WHERE entryDate <=" + time +
                                    "    UNION" +
                                    "    SELECT min(entryDate)" +
                                    "    FROM fillups" +
                                    "    WHERE entryDate >=" + time + ")";
                            fillUpDAO.getAsyncQueryHandler().startQuery(FillUpDAO.NEW_FILL_UP, fillUp, Uri.parse(url),
                                    FillUpDAO.ALL_COLUMNS, selectionClause, null, "entryDate ASC");
                        } else {
                            Log.d(TAG, "Missing fields from the Fill-Up.  Not doing anything.");
//                            displayToast("Something seems to be wrong with your values.  Please check your values.");
                        }
                    }

                    //Returns a null object if all the required fields are not present.
                    private FillUp createFillUpFromView() {
                        FillUp fillUp = new FillUp();

                        Date date = null;
                        try {
                            date = df.parse(dateValue.getText().toString());
                        } catch (ParseException e) {
                            displayToast("Date cannot be empty, or is invalid");
                            Log.e(TAG, "Error parsing date", e);
                        }

                        fillUp.setDate(date);
                        if (!levelBeforeFillValue.getText().toString().isEmpty()) {
                            fillUp.setLevelBeforeFillUp(Integer.valueOf(levelBeforeFillValue.getText().toString()));
                        } else {
                            displayToast("Tank % before fill-up cannot be empty");
                            return null;
                        }

                        if (!levelAfterFillValue.getText().toString().isEmpty()) {
                            fillUp.setLevelAfterFillUp(Integer.valueOf(levelAfterFillValue.getText().toString()));
                        } else {
                            displayToast("Tank % after fill-up cannot be empty");
                            return null;
                        }

                        if (!gallonsValue.getText().toString().isEmpty()) {
                            fillUp.setVolume(Float.valueOf(gallonsValue.getText().toString()));
                        } else {
                            displayToast("Volume cannot be empty");
                            return null;
                        }

                        if (!priceValue.getText().toString().isEmpty()) {
                            fillUp.setPrice(Float.valueOf(priceValue.getText().toString()));
                        } else {
                            displayToast("Price/g cannot be empty");
                            return null;
                        }


                        fillUp.setNotes(notesValue.getText().toString());

                        return fillUp;
                    }
                });

        return rootView;
    }


    @Override
    public void setDate(int year, int month, int day) {
        TextView dateValue = (TextView) this.getActivity().findViewById(R.id.fillUpDateValue);
        Log.d(TAG, "Setting date to " + month + "/" + day + "/" + year);
        dateValue.setText(month + "/" + day + "/" + year);
    }


    /**
     * This interface must be implemented by activities that contain this fragment to allow an interaction in this
     * fragment to be communicated to the activity and potentially other fragments contained in that activity.
     * <p/>
     * See the Android Training lesson <a href= "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void displayToast(String message) {
        Context context = this.getActivity().getApplicationContext();
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();


    }
}
