package com.crs.propanetracker;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.crs.propanetracker.com.crs.propanetracker.db.DbHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass. Activities that contain this fragment must implement the {@link
 * DailyEntryFragment.OnFragmentInteractionListener} interface to handle interaction events. Use the {@link
 * DailyEntryFragment#newInstance} factory method to create an instance of this fragment.
 */
public class DailyEntryFragment extends RootFragment {

    private static final String TAG = DailyEntryFragment.class.getSimpleName();
    private final SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    private OnFragmentInteractionListener mListener;


    /**
     * Use this factory method to create a new instance of this fragment using the provided parameters.
     *
     * @return A new instance of fragment DailyEntryFragment.
     */
    public static DailyEntryFragment newInstance() {
        DailyEntryFragment fragment = new DailyEntryFragment();
        return fragment;
    }

    public DailyEntryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        getFragmentManager().beginTransaction().add(this, "Daily");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        final TextView dateValue = (TextView) rootView.findViewById(R.id.dateFieldValue);
        final TextView levelValue = (TextView) rootView.findViewById(R.id.tankPercentValue);
        final TextView notesValue = (TextView) rootView.findViewById(R.id.notesValue);

        final DailyEntry dailyEntry = new DailyEntry();

        dateValue.setText(dailyEntry.getDateAsText());
        dateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment selectDateFragment = new SelectDateFragment();
                selectDateFragment.setTargetFragment(DailyEntryFragment.this, 0);
                selectDateFragment.show(getFragmentManager(), "DatePicker");
            }
        });


        rootView.findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Save button pressed");

                Log.d(TAG, "date: " + dateValue.getText());
                Log.d(TAG, "level: " + levelValue.getText().toString());
                String levelValueText = levelValue.getText().toString();

                if (!levelValueText.isEmpty()) {
                    int level = Integer.valueOf(levelValueText);
                    Date date = null;
                    try {
                        date = df.parse(dateValue.getText().toString());
                    } catch (ParseException e) {
                        Log.e(TAG, "Error parsing date", e);
                    }


                    DailyEntry newEntry = new DailyEntry();
                    newEntry.setLevel(level);
                    newEntry.setDate(date);
                    newEntry.setNotes(notesValue.getText().toString());

                    startAsyncQueryForDailyEntry(newEntry);
                } else {
                    displayToast("Tank % cannot be empty.");
                }
            }
        });


        return rootView;
    }

    public void startAsyncQueryForDailyEntry(DailyEntry newDailyEntry) {
        DailyEntryDAO dailyEntryDAO = new DailyEntryDAO(getContext());
        long time = newDailyEntry.getDate().getTime();

        dailyEntryDAO.getAsyncQueryHandler().startQuery(DailyEntryDAO.NEW_DAILY_ENTRY, newDailyEntry,
                Uri.parse(DailyEntryDAO.URL),
                DailyEntryDAO.ALL_COLUMNS, DbHelper.generateDailyEntrySelectionClause(time), null, "entryDate ASC");
    }


    private void displayToast(String message) {
        Context context = this.getActivity().getApplicationContext();
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "Detached.");
        mListener = null;
    }

    @Override
    public void setDate(int year, int month, int day) {
        TextView dateValue = (TextView) this.getActivity().findViewById(R.id.dateFieldValue);
        Log.d(TAG, "Setting date to " + month + "/" + day + "/" + year);
        dateValue.setText(month + "/" + day + "/" + year);
    }


    /**
     * This interface must be implemented by activities that contain this fragment to allow an interaction in this
     * fragment to be communicated to the activity and potentially other fragments contained in that activity.
     * <p/>
     * See the Android Training lesson <a href= "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


}
