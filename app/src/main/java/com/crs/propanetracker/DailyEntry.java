package com.crs.propanetracker;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class to capture a typical tank reading.
 */
public class DailyEntry implements Parcelable {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    private long id;
    private Calendar calendar = Calendar.getInstance();
    private Date date;
    private int level;
    private String notes;
    private boolean isEntryAfterFillUp = false;

    public DailyEntry() {
        date = new Date();
    }

    protected DailyEntry(Parcel in) {
        id = in.readLong();
        level = in.readInt();
        notes = in.readString();
    }


    public String getNotes() {

        return notes;
    }

    public void setNotes(String notes) {

        this.notes = notes;
    }

    public String getDateAsText() {

        return dateFormat.format(date);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {

        return date;
    }

    public void setDate(Date date) {

        this.date = date;
    }

    public void setDate(String date) throws ParseException {
        this.date = dateFormat.parse(date);
    }

    public int getLevel() {

        return level;
    }

    public void setLevel(int level) {

        this.level = level;
    }

    public Calendar getCalendar() {

        return calendar;
    }

    public void setCalendar(Calendar calendar) {

        this.calendar = calendar;
    }

    public void setIsEntryAfterFillUp(boolean entryAfterFillUp) {
        this.isEntryAfterFillUp = entryAfterFillUp;
    }

    public boolean isEntryAfterFillUp() {
        return isEntryAfterFillUp;
    }

    @Override
    public String toString() {
        return "DailyEntry{" +
                "\n  date=" + getDateAsText() +
                "\n  level=" + level +
                "\n  notes='" + notes + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeLong(id);
        parcel.writeLong(date.getTime());
        parcel.writeInt(level);
        parcel.writeString(notes);
    }

    public static final Creator<DailyEntry> CREATOR = new Creator<DailyEntry>() {
        @Override
        public DailyEntry createFromParcel(Parcel in) {
            return new DailyEntry(in);
        }

        @Override
        public DailyEntry[] newArray(int size) {
            return new DailyEntry[size];
        }
    };
}

