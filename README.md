# Propane Tracker

Propane Tracker allows for the tracking of home propane use.  Homeowners (or business owners) who have large 500-1,000 gallon propane tanks 
can benefit from tracking their propane usage.

### Building the app

What things you need to install the software and how to install them

```
examples
```

## Running the tests

It's easiest if you have some data to mess with.

1. Drag the DailyEntries.csv file into your emulator (this will automatically put it in your downloads folder)
2. In the app go to Settings-> Import data. (This will automatically import the data in the file you copied over)
3. Go to Charts to ensure you have data now. If you need more data to test with there are two files (small/full) in the directory. They need to be renamed to DailyEntries.csv and dropped into your emulator.

```
Example
```


## Built With

* [Gradle](https://maven.apache.org/) - Dependency Management

## Contributing

1. Fork it (<https://bitbucket.org/copperriversoftware/propanetracker/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

## Authors

* **Chad Beaudin** - *Initial work* - [GitHub](https://github.com/chadbeaudin)
* **Nate Thrasher** - *Initial work*  [BitBucket](https://github.com/chadbeaudin)https://bitbucket.org/iamthe1natron/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


